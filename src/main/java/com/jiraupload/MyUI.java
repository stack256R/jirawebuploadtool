package com.jiraupload;

import javax.servlet.annotation.WebServlet;

import com.thed.model.*;
import com.thed.service.impl.zie.TestcaseImportManagerImpl;
import com.thed.util.Constants;
import com.thed.util.Discriminator;
import com.thed.zfj.model.IssueType;
import com.thed.zfj.model.Project;
import com.thed.zfj.rest.JiraService;
import com.thed.zfj.rest.JiraServiceClass;
import com.thed.zfj.rest.ZfjServerType;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.server.*;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.jsoup.Jsoup;
import scala.collection.convert.WrapAsJava$;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@Widgetset("com.jiraupload.MyAppWidgetset")
@Push(PushMode.AUTOMATIC)
public class MyUI extends UI {

    private List<Project> projectsLst = null;
    private List<IssueType> issues = null;
    private TextField tf, tf1, strtRow, sheetFilter;
    private PasswordField tf2;
    private Panel fileUploadPanel, fileUploadPanel2, fConfigPnl, fConfigPn3, fConfigPn2;
    private ComboBox projects, issuetype, discr, imptAll, atchWkSht;
    private Table table;
    private Button jiraUpload, loginButton;
    private FormLayout excelData;
    private HashMap fieldConfigs;
    private ProgressBar cfgPrg, excelPrg;
    private DragAndDropWrapper configWrapper, excelWrapper;
    private ConfigFileHandlerWeb conf;
    private DropHandler dropHandler, dropHandler2;
    private StreamResource excelFileResource;
    private PopupView resultsView;
    private String fileName, fileExt;
    private VerticalLayout resultsLayout;
    private File fileObj;
    private ImportJob job;
    private VerticalLayout layout;
    private JiraServiceClass jiraServiceClass;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        buildMainLayout();
    }

    private void buildMainLayout() {
        job = null;
        conf = null;
        float tabWidth = 60.f;
        float txtHeight = 30.f;
        fieldConfigs = new LinkedHashMap<String,FieldConfig>();
        setUpDropHandler();
        excelFileResource = null;

        if(layout == null){
            layout = new VerticalLayout();
            setContent(layout);
        }else{ //refresh
            layout.removeAllComponents();
        }

        layout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        Label sTitle = new Label("Connection Settings");
        sTitle.setWidth(tabWidth,Unit.PERCENTAGE);

        layout.addComponent(sTitle);
        layout.setComponentAlignment(sTitle, Alignment.MIDDLE_CENTER);

        TabSheet tabsheet = new TabSheet();
        tabsheet.setWidth(tabWidth,Unit.PERCENTAGE);
        tabsheet.setHeight(290, Unit.PIXELS);

        layout.addComponent(tabsheet);
        layout.setComponentAlignment(tabsheet, Alignment.MIDDLE_CENTER);

        //Tab 1 Data
        FormLayout tab1 = new FormLayout();

        tf = new TextField("Jira URL : ");
        tf.setIcon(FontAwesome.CODE);
        tf.setRequired(true);
        tf.addValidator(new NullValidator("Must be provided to proceed",false));
        tf.setHeight(txtHeight, Unit.PIXELS);

        tf1 = new TextField("User ID :");
        tf1.setIcon(FontAwesome.USER);
        tf1.setRequired(true);
        tf1.addValidator(new NullValidator("Must be provided to proceed",false));
        tf1.setHeight(txtHeight, Unit.PIXELS);

        tf2 = new PasswordField("Password :");
        tf2.setIcon(FontAwesome.KEY);
        tf2.setRequired(true);
        tf2.addValidator(new NullValidator("Must be provided to proceed", false));
        tf2.setHeight(txtHeight, Unit.PIXELS);

        loginButton = new Button( "Connect" );
        loginButton.addStyleName(ValoTheme.BUTTON_FRIENDLY);

        tab1.addComponent(tf);
        tab1.addComponent(tf1);
        tab1.addComponent(tf2);
        tab1.addComponent(loginButton);
        tab1.setWidth(100, Unit.PERCENTAGE);
        tab1.setHeight(100, Unit.PERCENTAGE);

        //Tab 2 Data
        VerticalLayout tab2 = new VerticalLayout();
        tab2.setHeight(100, Unit.PERCENTAGE);
        tab2.setWidth(100, Unit.PERCENTAGE);

        fileUploadPanel = new Panel();
        fileUploadPanel.setWidth(100, Unit.PERCENTAGE);
        fileUploadPanel.setHeight(100, Unit.PERCENTAGE);

        Label panL  = new Label("Drag and Drop your configuration (.config) file here.");
        panL.setHeight(80, Unit.PERCENTAGE);
        panL.setWidth(100, Unit.PERCENTAGE);
        panL.addStyleName("v-label-customDragDrobLabel");

        VerticalLayout lw = new VerticalLayout();
        lw.setHeight(100, Unit.PERCENTAGE);
        lw.setWidth(100, Unit.PERCENTAGE);
        lw.addComponent(panL);

        //add progress bar
        cfgPrg = new ProgressBar();
        cfgPrg.setVisible(false);
        cfgPrg.setIndeterminate(true);
        cfgPrg.setHeight(20, Unit.PERCENTAGE);
        cfgPrg.setWidth(20, Unit.PERCENTAGE);

        lw.addComponent(cfgPrg);
        lw.setComponentAlignment(panL,Alignment.MIDDLE_CENTER);
        lw.setComponentAlignment(cfgPrg,Alignment.BOTTOM_CENTER);

        fileUploadPanel.setContent(lw);
        fileUploadPanel.addStyleName("customDragDropPanel");
        configWrapper = new DragAndDropWrapper(fileUploadPanel);
        configWrapper.setWidth(60, Unit.PERCENTAGE);
        configWrapper.setHeight(80, Unit.PERCENTAGE);
        configWrapper.setDragStartMode(DragAndDropWrapper.DragStartMode.HTML5);
        configWrapper.setDropHandler(dropHandler);
        //fileUploadPanel.setContent(configWrapper);

        tab2.addComponent(configWrapper);
        tab2.setComponentAlignment(configWrapper, Alignment.MIDDLE_CENTER);

        tabsheet.addTab(tab1,"Manual");
        tabsheet.addTab(tab2,"Config file");

        //setup for file and project setting
        Label sTitle1 = new Label("Project / File Settings");
        sTitle1.setWidth(tabWidth,Unit.PERCENTAGE);
        sTitle1.addStyleName("v-label-customHeader");

        layout.addComponent(sTitle1);
        layout.setComponentAlignment(sTitle1, Alignment.MIDDLE_CENTER);
        //layout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        fConfigPnl = new Panel();
        fConfigPnl.setWidth(tabWidth,Unit.PERCENTAGE);
        fConfigPnl.setHeight(240,Unit.PERCENTAGE);
        fConfigPnl.addStyleName("v-panel-splitCustom");

        HorizontalSplitPanel hsplit = new HorizontalSplitPanel();
        hsplit.setHeight(100,Unit.PERCENTAGE);
        hsplit.setWidth(100, Unit.PERCENTAGE);

        fConfigPnl.setContent(hsplit);

        Panel proj = new Panel("Project Settings");
        Panel exc = new Panel("Excel File Settings");
        proj.setWidth(100, Unit.PERCENTAGE);
        proj.setHeight(100, Unit.PERCENTAGE);
        exc.setWidth(100, Unit.PERCENTAGE);
        exc.setHeight(100, Unit.PERCENTAGE);

        proj.setStyleName(ValoTheme.PANEL_BORDERLESS);
        exc.setStyleName(ValoTheme.PANEL_BORDERLESS);

        //setup for project data
        FormLayout projData = new FormLayout();
        projData.setWidth(100, Unit.PERCENTAGE);
        projData.setHeight(100, Unit.PERCENTAGE);

        projects = new ComboBox("Project : ");
        issuetype = new ComboBox("Issue Type : ");

        projects.setFilteringMode(FilteringMode.STARTSWITH);
        issuetype.setFilteringMode(FilteringMode.STARTSWITH);

        projects.setHeight(txtHeight,Unit.PIXELS);
        //projects.setWidth(151,Unit.PIXELS);
        issuetype.setHeight(txtHeight,Unit.PIXELS);
        //issuetype.setWidth(151,Unit.PIXELS);

        issuetype.setInvalidAllowed(false);
        projects.setInvalidAllowed(false);
        projects.setNullSelectionAllowed(false);
        issuetype.setNullSelectionAllowed(false);

        issuetype.setRequired(true);
        projects.setRequired(true);
        projects.setIcon(FontAwesome.FOLDER);
        issuetype.setIcon(FontAwesome.FLASK);

        //populate projects
        //populate issueType

        projData.addComponent(projects);
        projData.addComponent(issuetype);
        proj.setContent(projData);

        //setup for excel file configuration
        excelData = new FormLayout();
        excelData.setWidth(100, Unit.PERCENTAGE);
        excelData.setHeight(100, Unit.PERCENTAGE);

        discr = new ComboBox("Discriminator : ");
        discr.setHeight(txtHeight,Unit.PIXELS);
        //discr.setWidth(151,Unit.PIXELS);

        discr.setInvalidAllowed(false);
        discr.setRequired(true);
        discr.setIcon(FontAwesome.GAVEL);
        discr.setFilteringMode(FilteringMode.STARTSWITH);
        discr.setNullSelectionAllowed(false);



        discr.addItem(Discriminator.BY_SHEET);
        discr.addItem(Discriminator.BY_EMPTY_ROW);
        discr.addItem(Discriminator.BY_ID_CHANGE);
        discr.addItem(Discriminator.BY_TESTCASE_NAME_CHANGE);
        discr.setValue(Discriminator.BY_TESTCASE_NAME_CHANGE);

        //Starting row
        strtRow = new TextField("Starting Row : ");
        strtRow.setHeight(txtHeight,Unit.PIXELS);
        strtRow.setIcon(FontAwesome.REORDER);
        strtRow.setConverter(Integer.class);
        strtRow.setValue("2");
        strtRow.setRequired(true);

        imptAll = new ComboBox("Import All Sheets : ");

        imptAll.setIcon(FontAwesome.GLOBE);
        imptAll.setInvalidAllowed(false);
        imptAll.setFilteringMode(FilteringMode.STARTSWITH);
        imptAll.setNullSelectionAllowed(false);
        imptAll.addItem(true);
        imptAll.addItem(false);
        imptAll.setValue(false);
        imptAll.setConverter(Boolean.class);
        imptAll.setHeight(txtHeight,Unit.PIXELS);

        sheetFilter = new TextField("Sheet Filter : ");
        sheetFilter.setHeight(txtHeight,Unit.PIXELS);
        sheetFilter.setIcon(FontAwesome.FILTER);
        sheetFilter.setEnabled(false);
        sheetFilter.setValue(".*");

        atchWkSht = new ComboBox("Attach worksheet To Issue : ");
        atchWkSht.setIcon(FontAwesome.FILE_EXCEL_O);
        atchWkSht.setInvalidAllowed(false);
        atchWkSht.setFilteringMode(FilteringMode.STARTSWITH);
        atchWkSht.setNullSelectionAllowed(false);
        atchWkSht.addItem(true);
        atchWkSht.addItem(false);
        atchWkSht.setValue(false);
        atchWkSht.setConverter(Boolean.class);
        atchWkSht.setHeight(txtHeight,Unit.PIXELS);

        excelData.addComponent(discr);
        excelData.addComponent(strtRow);
        excelData.addComponent(imptAll);
        excelData.addComponent(sheetFilter);
        excelData.addComponent(atchWkSht);
        excelData.setEnabled(false);

        exc.setContent(excelData);

        hsplit.setFirstComponent(proj);
        hsplit.setSecondComponent(exc);

        hsplit.setLocked(true);
        hsplit.setStyleName(ValoTheme.SPLITPANEL_LARGE);
        proj.addStyleName("customProjPanel");
        exc.addStyleName("customExcPanel");

        layout.addComponent(fConfigPnl);
        layout.setComponentAlignment(fConfigPnl, Alignment.MIDDLE_CENTER);

        //do jira excel field mapping
        Label sTitle2 = new Label("Excel - JIRA Field Mapping");
        sTitle2.setWidth(tabWidth,Unit.PERCENTAGE);
        sTitle2.addStyleName("v-label-customHeader");

        layout.addComponent(sTitle2);
        layout.setComponentAlignment(sTitle2, Alignment.MIDDLE_CENTER);

        fConfigPn2 = new Panel();
        fConfigPn2.setWidth(tabWidth,Unit.PERCENTAGE);
        fConfigPn2.setHeight(500,Unit.PIXELS);
        fConfigPn2.addStyleName("v-panel-splitCustom");

        table = new Table();
        table.setWidth(100, Unit.PERCENTAGE);
        table.setHeight(97, Unit.PERCENTAGE);

        table.setSelectable(false);

        // Define two columns for the built-in container
        //table.addContainerProperty("JIRA Field", Label.class, null);
        //table.addContainerProperty("Excel Column",  TextField.class, null);

        //Constants.excelFieldConfigs

        fConfigPn2.setContent(table);

        layout.addComponent(fConfigPn2);
        layout.setComponentAlignment(fConfigPn2, Alignment.MIDDLE_CENTER);

        //upload excel file
        Label sTitle3 = new Label("Excel File Upload");
        sTitle3.setWidth(tabWidth,Unit.PERCENTAGE);
        sTitle3.addStyleName("v-label-customHeader");

        layout.addComponent(sTitle3);
        layout.setComponentAlignment(sTitle3, Alignment.MIDDLE_CENTER);

        fConfigPn3 = new Panel();
        fConfigPn3.setWidth(tabWidth,Unit.PERCENTAGE);
        fConfigPn3.setHeight(240,Unit.PERCENTAGE);
        fConfigPn3.addStyleName("v-panel-splitCustom");


        VerticalLayout tab3 = new VerticalLayout();
        tab3.setHeight(100, Unit.PERCENTAGE);
        tab3.setWidth(100, Unit.PERCENTAGE);

        fileUploadPanel2 = new Panel();
        fileUploadPanel2.setWidth(100, Unit.PERCENTAGE);
        fileUploadPanel2.setHeight(100, Unit.PERCENTAGE);

        Label panL2  = new Label("Drag and Drop your excel (.xls, .xlsx, .xlsm) file here.");
        panL2.setHeight(100, Unit.PERCENTAGE);
        panL2.setWidth(100, Unit.PERCENTAGE);
        panL2.addStyleName("v-label-customDragDrobLabel");

        fileUploadPanel2.setContent(panL2);
        fileUploadPanel2.addStyleName("customDragDropPanel");

        excelWrapper = new DragAndDropWrapper(fileUploadPanel2);
        excelWrapper.setWidth(60, Unit.PERCENTAGE);
        excelWrapper.setHeight(240, Unit.PIXELS);
        excelWrapper.setDragStartMode(DragAndDropWrapper.DragStartMode.HTML5);
        excelWrapper.setDropHandler(dropHandler2);

        excelPrg = new ProgressBar();
        excelPrg.setVisible(false);
        excelPrg.setIndeterminate(true);
        excelPrg.setHeight(20, Unit.PERCENTAGE);
        excelPrg.setWidth(20, Unit.PERCENTAGE);

        tab3.addComponent(new Label("&nbsp;", ContentMode.HTML));
        tab3.addComponent(excelWrapper);
        tab3.addComponent(excelPrg);
        tab3.setComponentAlignment(excelWrapper, Alignment.MIDDLE_CENTER);
        tab3.setComponentAlignment(excelPrg, Alignment.BOTTOM_CENTER);

        fConfigPn3.setContent(tab3);

        layout.addComponent(fConfigPn3);
        layout.setComponentAlignment(fConfigPn3, Alignment.MIDDLE_CENTER);

        // import to Jira button
        jiraUpload = new Button( "Upload File To Jira" );
        jiraUpload.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        jiraUpload.setEnabled(false);

        //refresh button
        Button refreshButton = new Button("Refresh / Clear");
        refreshButton.addStyleName(ValoTheme.BUTTON_DANGER);
        refreshButton.setEnabled(true);

        //layout for submit buttons
        HorizontalLayout btnGp = new HorizontalLayout();
        btnGp.setWidth(40,Unit.PERCENTAGE);
        btnGp.setHeight(50,Unit.PIXELS);
        btnGp.addComponent(jiraUpload);
        btnGp.addComponent(refreshButton);

        layout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        //layout.addComponent(btnGp);
        //layout.setComponentAlignment(btnGp, Alignment.MIDDLE_CENTER);
        //layout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        //add popup with results
        resultsLayout = new VerticalLayout();
        resultsLayout.setWidth(100, Unit.PERCENTAGE);
        resultsLayout.setHeight(100, Unit.PERCENTAGE);

        resultsView = new PopupView("View Upload Results", resultsLayout);
        btnGp.addComponent(resultsView);
        btnGp.setComponentAlignment(jiraUpload,Alignment.MIDDLE_LEFT);
        btnGp.setComponentAlignment(refreshButton,Alignment.MIDDLE_CENTER);
        btnGp.setComponentAlignment(resultsView,Alignment.MIDDLE_LEFT);

        layout.addComponent(btnGp);
        layout.setComponentAlignment(btnGp, Alignment.MIDDLE_CENTER);
        layout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        //Logic handling
        refreshButton.addClickListener(clickEvent -> {
            buildMainLayout();
        });

        //to connect to jira
        loginButton.addClickListener(clickEvent -> {
            if (tf.getValue().isEmpty() || tf1.getValue().isEmpty() || tf2.getValue().isEmpty()){
                StringBuilder stringBuilder = new StringBuilder();
                if (tf.getValue().isEmpty()){
                    stringBuilder.append("\n Jira URL is a required field");
                }
                if (tf1.getValue().isEmpty()){
                    stringBuilder.append("\n User Name is a required field");
                }
                if (tf2.getValue().isEmpty()){
                    stringBuilder.append("\n Password is a required field");
                }
                showErrorNotification(stringBuilder.toString());
            }else{ // attempt login
                processLogin(tf.getValue(),tf1.getValue(),tf2.getValue());
            }
        });

        //when a project is selected list issues.
        projects.addValueChangeListener(valueChangeEvent -> {
            if (valueChangeEvent.getProperty().getValue() != null) {
                processIssueType((Project) valueChangeEvent.getProperty().getValue());
            }
        });

        //when an issue is selected, we then enable the excel file
        issuetype.addValueChangeListener(valueChangeEvent -> {
            excelData.setEnabled(valueChangeEvent.getProperty().getValue() != null);
            if (null != valueChangeEvent.getProperty().getValue()){
                setUpMyExcelFields((IssueType) valueChangeEvent.getProperty().getValue());
            }else{
                cfgPrg.setVisible(false);
            }
        });

        //when the import all sheet option is selected, we activate the sheet filter text field
        imptAll.addValueChangeListener(valueChangeEvent -> {
            try{
                boolean toReturn = (boolean) imptAll.getConvertedValue();
                sheetFilter.setEnabled(toReturn);
            }catch (Converter.ConversionException e){
                Notification.show("Invalid 'Import All Sheets'");
            }
        });

        //when the user has gotten this far and choose to upload their file.
        jiraUpload.addClickListener(clickEvent -> {
            processAndUploadToJira();
        });
        setEnabledForMany(false);
    }

    private void setEnabledForMany(Boolean val){
        fConfigPnl.setEnabled(val);
        fConfigPn3.setEnabled(val);
        fConfigPn2.setEnabled(val);
    }

    private void setEnabledForSome(Boolean val){
        issuetype.setEnabled(val);
    }

    //function to update excel fields
    private void setUpMyExcelFields(IssueType selIssue){
        //find selected issue
        fieldConfigs.clear();
        fieldConfigs.putAll(Constants.excelFieldConfigs);
        Map<String,Object> fields = WrapAsJava$.MODULE$.mapAsJavaMap(selIssue.fields());
        fields.forEach((k,v) -> {
            if(k.startsWith("customfield")){
                Map<String, Object> vMap = WrapAsJava$.MODULE$.mapAsJavaMap((scala.collection.immutable.HashMap)v);
                String fieldMetadataId = populateFieldTypes(WrapAsJava$.MODULE$.mapAsJavaMap((scala.collection.immutable.HashMap)v));
                String desc = vMap.get("name").toString();
                Boolean required = Boolean.getBoolean(vMap.get("required").toString());
                if (required) desc += " *";
                List temp;
                if (vMap.get("allowedValues") != null){
                    if(vMap.get("allowedValues") instanceof scala.collection.immutable.Seq){
                        scala.collection.immutable.Seq obj = (scala.collection.immutable.Seq)vMap.get("allowedValues");
                        temp = WrapAsJava$.MODULE$.seqAsJavaList(obj);
                    }else{
                        temp = new ArrayList();
                    }
                }else{
                    temp = new ArrayList();
                }
                if (!desc.contains("Zephyr")){
                    fieldConfigs.put(k,new FieldConfig(k,"testcase",false,fieldMetadataId,k,k,desc,"This is "+ desc,required,true,true,true,255,temp));
                }
            }
        });
        //after this we populate the jira field map table.
        IndexedContainer container = new IndexedContainer();

        // table headers
        container.addContainerProperty("JIRA Field", Label.class, null);
        container.addContainerProperty("Excel Column", TextField.class, null);

        // Now fill the container with my hashmap (objectMetadatas) and at the end we will add the container to the table
        fieldConfigs.forEach((k,v)->{
            Integer rowId = new Integer(container.size());
            container.addItem(rowId);
            FieldConfig temp = (FieldConfig)v;
            container.getContainerProperty(rowId, "JIRA Field").setValue(new Label(temp.getDisplayName()));
            TextField tmp = new TextField();
            if (conf != null){
                try{
                    String val = conf.getIssueField(cleanString(temp.getDisplayName()));
                    tmp.setValue(val);
                }catch (Exception e){
                    //cfgPrg.setVisible(false);
                }
            }
            container.getContainerProperty(rowId, "Excel Column").setValue(tmp);
        });
        // Finally do not forget to add that container to your table
//        container.addValueChangeListener(valueChangeEvent -> {
//
//        });
        table.setContainerDataSource(container);
        cfgPrg.setVisible(false);
        checkShouldUpload();
    }

    private String populateFieldTypes(Map<String, Object> fldVal){
        Map <String, Object> customFldMetadata = WrapAsJava$.MODULE$.mapAsJavaMap((scala.collection.immutable.Map)fldVal.get("schema"));
        Object rtn = customFldMetadata.get("type");
        String jiraDataType = "";
        if(rtn instanceof String){
            jiraDataType = (String)rtn;
        }
        String itemDataType = "";
        if (jiraDataType.equalsIgnoreCase("array") && customFldMetadata.containsKey("items")){
            rtn = customFldMetadata.get("items");
            itemDataType = (String)rtn;
        }
        jiraDataType = jiraDataType.equalsIgnoreCase("any")? "string":jiraDataType;
        String customType = (String)customFldMetadata.getOrDefault("custom","");
        String fieldMetadataId = jiraDataType + ":" + itemDataType + ":"+ customType;
        FieldTypeMetadata fieldMetadata = new FieldTypeMetadata(fieldMetadataId,"Text (1024)",jiraDataType,itemDataType,customType,1024,true,100);
        Constants.fieldTypeMetadataMap.put(fieldMetadataId,fieldMetadata);
        return fieldMetadataId;
    }

    private void processIssueType(Project sel){
        if (sel != null){
            try{
                prepForUpload();
                List<Project> prj = WrapAsJava$.MODULE$.seqAsJavaList(JiraService.getMeta(sel.id()));
                issues = WrapAsJava$.MODULE$.seqAsJavaList(prj.get(0).issuetypes());
                BeanItemContainer<IssueType> container = new BeanItemContainer<IssueType>(IssueType.class);
                container.addAll(issues);
                issuetype.setContainerDataSource(container);
                issuetype.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
                issuetype.setItemCaptionPropertyId("name");
                setEnabledForSome(true);
                if (conf != null){
                    setUpIssues();
                }
                checkShouldUpload();
            }catch(Exception ex){
                cfgPrg.setVisible(false);
                showErrorNotification(ex.getMessage());
            }
        }
    }

    private void processLogin(String url, String userName, String pwd){
        JiraService.url_base_$eq(url);
        JiraService.userName_$eq(userName);
        JiraService.passwd_$eq(pwd);
        try{
            projectsLst = WrapAsJava$.MODULE$.seqAsJavaList(JiraService.getProjects());
            if (!(projectsLst == null || projectsLst.isEmpty())){
                BeanItemContainer<Project> container = new BeanItemContainer<Project>(Project.class);
                container.addAll(projectsLst);
                projects.setContainerDataSource(container);
                projects.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
                projects.setItemCaptionPropertyId("name");
                setEnabledForMany(true);
                setEnabledForSome(false);
                checkShouldUpload();
                if (conf != null){
                    setUpProject();
                }
            }
        }catch(Exception ex){
            cfgPrg.setVisible(false);
            showErrorNotification(ex.getMessage());
        }
    }

    private void showErrorNotification(String message){
        Notification notf = new Notification(message, "Error Notification",Notification.Type.ERROR_MESSAGE, true);
        notf.setDelayMsec(5000);
        notf.show(this.getPage());
    }

    private void showGoodMessage(String message){
        Notification notf = new Notification(message, Notification.Type.HUMANIZED_MESSAGE);
        notf.setDelayMsec(10000);
        notf.show(this.getPage());
    }

    //create stream resource from upload
    private StreamResource createStreamResource(String name, String type, final ByteArrayOutputStream bas) {
        // resource for serving the file contents
        StreamResource.StreamSource streamSource = new StreamResource.StreamSource() {
            public InputStream getStream() {
                if (bas != null) {
                    byte[] byteArray = bas.toByteArray();
                    return new ByteArrayInputStream(byteArray);
                }
                return null;
            }
        };
        StreamResource resource = new StreamResource(streamSource, name);
        return resource;
    }

    private void setUpProject(){
        if (projectsLst != null){
            //on successful login we proceed to select the project
            projectsLst.forEach(project -> {
                if (StringUtils.equals(project.key(), conf.PROJECT_KEY)){
                    projects.setValue(project);
                    //processIssueType(project);
                }
            });
            if (projects.getValue() == null){
                cfgPrg.setVisible(false);
            }
            checkShouldUpload();
        }else{
            cfgPrg.setVisible(false);
        }
    }

    private void setUpIssues(){
        //on successful project selection we proceed to load the possible issue type
        if (issues != null){
            issues.forEach(issType -> {
                issuetype.setValue(issType);
                //setUpMyExcelFields(issType);
            });
            if (issuetype.getValue() == null){
                cfgPrg.setVisible(false);
            }
            checkShouldUpload();
        }else {
            cfgPrg.setVisible(false);
        }
    }

    //file upload for config file
    private void loadConfigFileAndSetUp(StreamResource resource, final ByteArrayOutputStream byteArrayOutputStream){
        conf = new ConfigFileHandlerWeb(resource.getStreamSource().getStream());
        //logon details
        tf.setValue(conf.CONNECTION_URL);
        tf1.setValue(conf.CONNECTION_USERNAME);
        tf2.setValue(conf.CONNECTION_PASSWORD);

        //if username, password and url then login
        if (!(isEmptyOrWhitespace(conf.CONNECTION_URL) || isEmptyOrWhitespace(conf.CONNECTION_USERNAME) || isEmptyOrWhitespace(conf.CONNECTION_PASSWORD))) {
            processLogin(conf.CONNECTION_URL,conf.CONNECTION_USERNAME,conf.CONNECTION_PASSWORD);
            //we load all remaining fields from file.
            if (!isEmptyOrWhitespace(conf.getString("excel.discriminator"))){
                switch (conf.getString("excel.discriminator").toLowerCase()){
                    case "by sheet":
                        discr.setValue(Discriminator.BY_SHEET);
                        break;
                    case "by empty row":
                        discr.setValue(Discriminator.BY_EMPTY_ROW);
                        break;
                    case "by id change":
                        discr.setValue(Discriminator.BY_ID_CHANGE);
                        break;
                    case "by testcase name change":
                        discr.setValue(Discriminator.BY_TESTCASE_NAME_CHANGE);
                        break;
                }
            }
            //map remaing fields
            String tp = conf.getString("excel.row");
            strtRow.setValue(isEmptyOrWhitespace(tp)? "2":tp);
            imptAll.setValue(conf.getBoolean("excel.sheets.all"));
            tp = conf.getString("excel.sheets.filter");
            sheetFilter.setValue(isEmptyOrWhitespace(tp)? "2":tp);
            atchWkSht.setValue(conf.getBoolean("excel.attach"));
        }else{
            cfgPrg.setVisible(false);
            showErrorNotification("Some required fields are not available in the config file");
        }

    }

    private String getFileExtension(String fileType){
        String toReturn = "";
        switch (fileType){
            case "application/vnd.ms-excel.addin.macroEnabled.12":
                toReturn = ".xlam";
                break;
            case "application/vnd.ms-excel":
                toReturn = ".xls";
                break;
            case "application/vnd.ms-excel.sheet.binary.macroEnabled.12":
                toReturn = ".xlsb";
                break;
            case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                toReturn = ".xlsx";
                break;
            case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":
                toReturn = ".xltx";
                break;
            case "application/vnd.ms-excel.sheet.macroEnabled.12":
                toReturn = ".xlsm";
                break;
        }
        return toReturn;
    }

    private void setUpDropHandler(){

        dropHandler = new DropHandler() {
            @Override
            public void drop(DragAndDropEvent dragAndDropEvent) {
                // expecting this to be an html5 drag
                DragAndDropWrapper.WrapperTransferable tr = (DragAndDropWrapper.WrapperTransferable) dragAndDropEvent.getTransferable();
                Html5File[] files = tr.getFiles();
                if (!(files == null || files.length < 1)) {
                    final Html5File html5File = files[0];
                    final ByteArrayOutputStream bas = new ByteArrayOutputStream();
                    StreamVariable streamVariable = new StreamVariable() {
                        public OutputStream getOutputStream() {
                            return bas;
                        }
                        public boolean listenProgress() {
                            return false;
                        }
                        public void onProgress(StreamingProgressEvent event) {}
                        public void streamingStarted(StreamingStartEvent event) {}
                        public void streamingFinished(StreamingEndEvent event) {
                            String text = html5File.getFileName() +" has been uploaded successfully.";
                            showGoodMessage(text);
                            //begin processing config file
                            loadConfigFileAndSetUp(createStreamResource(html5File.getFileName(),html5File.getType(),bas),bas);
                        }
                        public void streamingFailed(StreamingErrorEvent event) {
                            cfgPrg.setVisible(false);
                            showErrorNotification("File Upload failed, please try again.");
                        }
                        public boolean isInterrupted() {
                            return false;
                        }
                    };
                    html5File.setStreamVariable(streamVariable);
                    cfgPrg.setVisible(true);
                }else {
                    String text = tr.getText();
                    if (text != null) {
                        showErrorNotification(text);
                    }
                }
            }

            @Override
            public AcceptCriterion getAcceptCriterion() {
                return AcceptAll.get();
            }
        };

        dropHandler2 = new DropHandler() {
            @Override
            public void drop(DragAndDropEvent dragAndDropEvent) {
                // expecting this to be an html5 drag
                DragAndDropWrapper.WrapperTransferable tr = (DragAndDropWrapper.WrapperTransferable) dragAndDropEvent.getTransferable();
                Html5File[] files = tr.getFiles();
                if (!(files == null || files.length < 1)) {
                    List<String> excelTypes = Arrays.asList("application/vnd.ms-excel.addin.macroEnabled.12", "application/vnd.ms-excel.sheet.macroEnabled.12",
                            "application/vnd.ms-excel", "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
                    final Html5File html5File = files[0];
                    //System.err.println(html5File.getType());
                    if(excelTypes.contains(html5File.getType())){
                        fileExt = getFileExtension(html5File.getType());
                        fileName = html5File.getFileName();
                        final ByteArrayOutputStream bas = new ByteArrayOutputStream();
                        StreamVariable streamVariable = new StreamVariable() {
                            public OutputStream getOutputStream() {
                                return bas;
                            }
                            public boolean listenProgress() {
                                return false;
                            }
                            public void onProgress(StreamingProgressEvent event) {}
                            public void streamingStarted(StreamingStartEvent event) {}
                            public void streamingFinished(StreamingEndEvent event) {
                                String text = html5File.getFileName() +" has been uploaded to server successfully. Pending user action to upload to JIRA.";
                                showGoodMessage(text);
                                excelPrg.setVisible(false);
                                excelFileResource = createStreamResource(html5File.getFileName(),html5File.getType(),bas);
                                checkShouldUpload();
                            }
                            public void streamingFailed(StreamingErrorEvent event) {
                                excelPrg.setVisible(false);
                                showErrorNotification("File Upload failed, please try again.");
                            }
                            public boolean isInterrupted() {
                                return false;
                            }
                        };
                        html5File.setStreamVariable(streamVariable);
                        excelPrg.setVisible(true);
                    }else{
                        showErrorNotification("Only Excel Files Accepted");
                        return;
                    }
                }else {
                    String text = tr.getText();
                    if (text != null) {
                        showErrorNotification(text);
                    }
                }
            }

            @Override
            public AcceptCriterion getAcceptCriterion() {
                return AcceptAll.get();
            }
        };
    }

    private void checkShouldUpload(){
        jiraUpload.setEnabled((excelFileResource !=null) && fConfigPnl.isEnabled() && fConfigPn2.isEnabled() && fConfigPn3.isEnabled());
    }

    private String findFieldWithName(final Map<String, String> cmatch){
        String toReturn = null;
        fieldConfigs.forEach((fieldID,fieldObj) ->{
            cmatch.put(((FieldConfig)fieldObj).getDisplayName(),(String) fieldID);
        });
        return  toReturn;
    }

    private void processAndUploadToJira(){
        if (jiraUpload.isEnabled()){
            excelPrg.setVisible(true);
            jiraServiceClass = new JiraServiceClass(tf.getValue(), tf1.getValue(),tf2.getValue(),(Project) projects.getValue(),(IssueType) issuetype.getValue(),null,ZfjServerType.BTF());
            //JiraService.serverType_$eq(ZfjServerType.BTF());
            //JiraService.zConfig_$eq(null);
            HashSet fieldMpaDetails = new HashSet<FieldMapDetail>();
            //loop through table and get current data values.
            Label tmpL;
            TextField textField;
            final Map<String, String> cmatch = new LinkedHashMap<String,String>();
            findFieldWithName(cmatch);
            for (Iterator i = table.getItemIds().iterator(); i.hasNext();){
                Item item = table.getItem(i.next());
                tmpL = (Label) item.getItemProperty("JIRA Field").getValue();
                textField = (TextField) item.getItemProperty("Excel Column").getValue();
                fieldMpaDetails.add(new FieldMapDetail(cmatch.get(tmpL.getValue()),textField.getValue()));
            }

            //create a fieldMap object
            FieldMap fieldMap = new FieldMap(Integer.parseInt(strtRow.getValue()),(Discriminator)discr.getValue(),fieldMpaDetails);
            //resultsView
            try{
                //create file in temp directory
                fileObj = File.createTempFile(fileName,fileExt);
                OutputStream outputStream = new FileOutputStream(fileObj);
                IOUtils.copy(excelFileResource.getStreamSource().getStream(), outputStream);
                outputStream.close();

                HashSet<JobHistory> jobHistories = new LinkedHashSet<JobHistory>();
                final scala.Option<String> fieldVal = scala.Option.apply(((Boolean)imptAll.getValue())? null: sheetFilter.getValue());//.filter(p -> (!StringUtil.isEmptyOrWhitespace(p) && (Boolean)imptAll.getValue()));
                //Option <String> fieldVal =  Option.apply(sheetFilter.getValue()).filter(p -> (!StringUtil.isEmptyOrWhitespace(p) && (Boolean)imptAll.getValue()));

                job = new ImportJob(fileObj.getAbsolutePath(),fieldMap,fieldConfigs,jobHistories,fieldVal);
                //CustomImportJob job = new CustomImportJob(,fileName,fieldMap,fieldConfigs,jobHistories,fieldVal)
                job.setAttachFile((Boolean) atchWkSht.getValue());
                ExecutorService executorService = Executors.newSingleThreadExecutor();
                executorService.submit(()->{
                    //CustomTestcaseImportManagerImpl importManager = new CustomTestcaseImportManagerImpl();
                    //JiraService.project_$eq((Project) projects.getValue());
                    //JiraService.issueType_$eq((IssueType) issuetype.getValue());
                    //prepForUpload();
                    //importManager.importAllFiles()
                    //this.getUI().setPollInterval(1000);
                    JiraService.startImport(job, new TestcaseImportManagerImpl(jiraServiceClass));
                    excelFileResource = null;
                    this.getUI().access(()->{
                        upDateUI();
                    });
                    //this.getUI().setPollInterval(0);
                });
            }catch (Exception exc){
                excelPrg.setVisible(false);
                showErrorNotification(exc.getMessage());
            }

        }
    }

    private void upDateUI(){
        checkShouldUpload(); //disable
        excelPrg.setVisible(false);
        if (job !=null){
            String result = job.getHistory().toString();
            result = Jsoup.parse(result).text();
            result = WordUtils.wrap(result,90);
            resultsLayout.removeAllComponents();
            Label rslt = new Label();
            rslt.setValue(result);
            resultsLayout.addComponent(rslt);
            resultsLayout.setComponentAlignment(rslt,Alignment.MIDDLE_CENTER);
            if (StringUtils.containsIgnoreCase(result,"error")){
                showErrorNotification(result);
            }else{
                //show popup with data
                resultsView.setHideOnMouseOut(false);
                resultsView.setSizeFull();
                resultsView.setPopupVisible(true);
            }
        }
    }

    private void prepForUpload(){
        JiraService.url_base_$eq(tf.getValue());
        JiraService.userName_$eq(tf1.getValue());
        JiraService.passwd_$eq(tf2.getValue());
    }

    private String cleanString(String val){
        String toReturn  = val;
        if(!isEmptyOrWhitespace(val)){
            toReturn = val.replace(" *","").replaceAll("(\\s+)" + "((?:[a-z][a-z]+))","").replaceAll("(\\[.*?\\])","").trim();
        }
        return toReturn;
    }

    private boolean isEmptyOrWhitespace(String val){
        return (StringUtils.isEmpty(val) || StringUtils.isBlank(val));
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = true)
    public static class MyUIServlet extends VaadinServlet {
    }
}
