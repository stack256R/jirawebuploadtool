package com.jiraupload;

import com.thed.util.ConfigFileHandler;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by robert.j.ssemmanda on 27/06/2016.
 */
public class ConfigFileHandlerWeb {

    private static final String CONNECTION_URL_PROPERTY = "connection.url";
    private static final String CONNECTION_CLOUD_PROPERTY = "connection.cloud";
    private static final String CONNECTION_USERNAME_PROPERTY = "connection.username";
    private static final String CONNECTION_PASSWORD_PROPERTY = "connection.password";
    private static final String PROJECT_KEY_PROPERTY = "project.key";
    private static final String PROJECT_ISSUE_TYPE_PROPERTY = "project.issue.type";
    private static final Log logger = LogFactory.getLog(ConfigFileHandler.class);
    public final String CONNECTION_URL;
    public final boolean CONNECTION_CLOUD;
    public final String CONNECTION_USERNAME;
    public final String CONNECTION_PASSWORD;
    public final String PROJECT_KEY;
    public final String PROJECT_ISSUE_TYPE;
    private final Config conf;

    public ConfigFileHandlerWeb(InputStream var1) {



        InputStreamReader r = new InputStreamReader(var1);
        this.conf = ConfigFactory.parseReader(r);
        logger.debug(this.conf);
        this.CONNECTION_URL = this.getString("connection.url");
        this.CONNECTION_CLOUD = this.getBoolean("connection.cloud");
        this.CONNECTION_USERNAME = this.getString("connection.username");
        this.CONNECTION_PASSWORD = this.getString("connection.password");
        this.PROJECT_ISSUE_TYPE = this.getString("project.issue.type");
        this.PROJECT_KEY = this.getString("project.key");
        String var2 = String.format("Url %s, is cloud? %s, Usr:%s, psw: %s", new Object[]{this.CONNECTION_URL, Boolean.valueOf(this.CONNECTION_CLOUD), this.CONNECTION_USERNAME, this.CONNECTION_PASSWORD});
        logger.debug("loaded server config: " + var2);
    }


    public String getIssueField(String var1) {
        return this.getString("project.issue.fields." + var1);
    }

    protected boolean getBoolean(String var1) {
        if(this.conf.hasPath(var1)) {
            return this.conf.getBoolean(var1);
        } else {
            logger.warn("no such property:" + var1);
            return false;
        }
    }

    protected String getString(String var1) {
        if(this.conf.hasPath(var1)) {
            return this.conf.getString(var1);
        } else {
            logger.warn("no such property:" + var1);
            return "";
        }
    }

    protected int getInt(String var1) {
        if(this.conf.hasPath(var1)) {
            return this.conf.getInt(var1);
        } else {
            logger.warn("no such property:" + var1);
            return -1;
        }
    }

}
